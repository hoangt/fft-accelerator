//module TM_SRAM32_2RW_32x768b 
module tmem
#(
	parameter   num_bank = 32
)
(
	//--- Port 1
		input 								CE1,
		input 								WEB1,
		input 								OEB1,
		input 								CSB1,
		input 	[5:0] 						A1,
		input 	[24*num_bank-1:0] 		I1,
		output 	[24*num_bank-1:0] 		O1,

	//--- Port 2
		input 								CE2,
		input 								WEB2,
		input 								OEB2,
		input 								CSB2,
		input 	[5:0] 						A2,
		input 	[24*num_bank-1:0] 		I2,
		output 	[24*num_bank-1:0]		O2
);

	genvar	   i;
	generate
		for (i=0; i<num_bank; i=i+1) begin
			SRAM2RW64x16 	inst_SRAM2RW64x16
			(
				.CE1(CE1), .WEB1(WEB1), .OEB1(OEB1), .CSB1(CSB1), .A1(A1), .I1(I1[24*i + 15:24*i]), .O1(O1[24*i + 15:24*i]),
				.CE2(CE2), .WEB2(WEB2), .OEB2(OEB2), .CSB2(CSB2), .A2(A2), .I2(I2[24*i + 15:24*i]), .O2(O2[24*i + 15:24*i])
			);

			SRAM2RW64x8	 	inst_SRAM2RW64x8
			(
				.CE1(CE1), .WEB1(WEB1), .OEB1(OEB1), .CSB1(CSB1), .A1(A1), .I1(I1[24*i + 23:24*i+16]), .O1(O1[24*i + 23:24*i+16]),
				.CE2(CE2), .WEB2(WEB2), .OEB2(OEB2), .CSB2(CSB2), .A2(A2), .I2(I2[24*i + 23:24*i+16]), .O2(O2[24*i + 23:24*i+16])
			);

		end
	endgenerate

endmodule
