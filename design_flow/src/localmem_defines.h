//====================================================================================================-=
// Data MEMory (DMEM)
// Full address     =  | `DM_BANK_ADDR | DM_BANK_SELECT  |
//====================================================================================================-=
	`define DM_ROWS    				32													
	`define DM_COLS    				64*3*8												// in bits = 1536 bits
																							// total DMEM size = DM_ROWS * DM_COLS = 6 kBytes 
	
	`define DM_BANK_COLS    			96													// in bits
	`define DM_BANK_ROWS    			`DM_ROWS													
	`define DM_BANK_ADDR			5													// log2(DM_BANK_ROWS)
	`define DM_BANK_NUM				`DM_COLS / `DM_BANK_COLS				
	`define DM_BANK_SELECT			4													// log2(DM_BANK_NUM	

	`define DM_ADDR 					(`DM_BANK_SELECT + `DM_BANK_ADDR)			

//====================================================================================================-=
// Twiddle Factor MEMory (DMEM)
// Full address     =  | `FM_BANK_ADDR | FM_BANK_SELECT  |
//====================================================================================================-=
	`define FM_ROWS    				16													
	`define FM_COLS    					32*3*8												// in bits = 768 bits
														 									// total FMEM size = FM_ROWS * FM_COLS = 1.5 kBytes 
	
	`define FM_BANK_COLS    			96													// same as DM_BANK_COLS
	`define FM_BANK_ROWS    			`FM_ROWS													
	`define FM_BANK_ADDR				4													// log2(FM_BANK_ROWS)
	`define FM_BANK_NUM				`FM_COLS / `FM_BANK_COLS				
	`define FM_BANK_SELECT			4													// log2(FM_BANK_NUM	

	`define FM_ADDR 					(`FM_BANK_SELECT + `FM_BANK_ADDR)			

//====================================================================================================-=
// Transposition MEMory (TMEM)
// Full address     =  | `TM_BANK_ADDR | TM_BANK_SELECT  |
//====================================================================================================-=
	`define TM_ROWS    				16													
	`define TM_COLS    					32*3*8												// in bits = 768 bits
														 									// total TMEM size = TM_ROWS * TM_COLS = 1.5 kBytes 
	
	`define TM_BANK_COLS    			96													// same as DM_BANK_COLS
	`define TM_BANK_ROWS    			`TM_ROWS													
	`define TM_BANK_ADDR				4													// log2(TM_BANK_ROWS)
	`define TM_BANK_NUM				`TM_COLS / `TM_BANK_COLS				
	`define TM_BANK_SELECT			4													// log2(TM_BANK_NUM	

	`define TM_ADDR 					(`TM_BANK_SELECT + `TM_BANK_ADDR)			

//====================================================================================================-=
// Option for testbench
// 
//====================================================================================================-=
`ifdef	SIM_DMEM
	`define MEM_ROWS    				`DM_ROWS													
	`define MEM_COLS    				`DM_COLS
	`define MEM_BANK_COLS    		`DM_BANK_COLS
	`define MEM_BANK_ROWS    		`DM_BANK_ROWS
	`define MEM_BANK_ADDR    		`DM_BANK_ADDR
	`define MEM_BANK_NUM			`DM_BANK_NUM
	`define MEM_BANK_SELECT			`DM_BANK_SELECT	
	`define MEM_ADDR 					`DM_ADDR

`elsif	SIM_FMEM
	`define MEM_ROWS    				`FM_ROWS													
	`define MEM_COLS    				`FM_COLS
	`define MEM_BANK_COLS    		`FM_BANK_COLS
	`define MEM_BANK_ROWS    		`FM_BANK_ROWS
	`define MEM_BANK_ADDR    		`FM_BANK_ADDR
	`define MEM_BANK_NUM			`FM_BANK_NUM
	`define MEM_BANK_SELECT			`FM_BANK_SELECT
	`define MEM_ADDR 					`FM_ADDR

`elsif	SIM_TMEM
	`define MEM_ROWS    				`TM_ROWS													
	`define MEM_COLS    				`TM_COLS
	`define MEM_BANK_COLS    		`TM_BANK_COLS
	`define MEM_BANK_ROWS    		`TM_BANK_ROWS
	`define MEM_BANK_ADDR    		`TM_BANK_ADDR
	`define MEM_BANK_NUM			`TM_BANK_NUM
	`define MEM_BANK_SELECT			`TM_BANK_SELECT
	`define MEM_ADDR 					`TM_ADDR

`else
	//  NOTHING
`endif
