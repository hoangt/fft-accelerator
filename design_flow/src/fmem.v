`include "localmem_defines.h"

//======================================================
//======================================================
module fmem_bank
(
	//--- Port 1
		input 										CE1,
		input 										WEB1,
		input 										OEB1,
		input 										CSB1,
		input 	[`FM_BANK_ADDR-1:0] 				A1,
		input 	[`FM_BANK_COLS-1:0] 				I1,
		output 	[`FM_BANK_COLS-1:0] 				O1
);

	localparam		bus_size	= 32;				// equal to SRAM2RW16x32 data bus 
	genvar 			i;

	generate 
		for (i=0; i<`FM_BANK_COLS/bus_size; i=i+1) begin: gen_sram
			SRAM2RW16x32  inst_SRAM2RW16x32 		
			(
					.CE1(CE1), .WEB1(WEB1), .OEB1(OEB1), .CSB1(CSB1),  .A1(A1), 
					.I1(I1[(i+1)*bus_size-1:i*bus_size]), .O1(O1[(i+1)*bus_size-1:i*bus_size])
			);			
		end
	endgenerate

endmodule // dmem_bank


//======================================================
//======================================================
module fmem
(
	//--- Port 1
		input 								CE1,
		input 								WEB1,
		input 								OEB1,
		input 								CSB1,
		input 	[`FM_ADDR-1:0] 			A1,
		input 	[`FM_COLS-1:0] 			I1,
		output 	[`FM_COLS-1:0] 			O1,
		output 	[`FM_BANK_COLS-1:0] 		SEQ_O1
);

	reg 		[`FM_BANK_SELECT-1:0]	BANK_SEL1;
	wire		[`FM_COLS-1:0] 			wireO1, wireO2;

	genvar 	   	i;

	//assign 	BANK_SEL1 = A1[`FM_BANK_SELECT-1:0];
	//assign 	BANK_SEL2 = A2[`FM_BANK_SELECT-1:0];

	always @(posedge CE1)
		BANK_SEL1 <= A1[`FM_BANK_SELECT-1:0];

	generate
		for (i=0; i<`FM_BANK_NUM; i=i+1) begin: gen_dmem_bank
			fmem_bank  inst_fmem_bank
			(
				.CE1(CE1), .WEB1(WEB1), .OEB1(OEB1), .CSB1(CSB1),  .A1(A1[`FM_ADDR-1:`FM_BANK_SELECT]), 
				.I1(I1[(i+1)*`FM_BANK_COLS-1:i*`FM_BANK_COLS]), 
				.O1(wireO1[(i+1)*`FM_BANK_COLS-1:i*`FM_BANK_COLS])
			);
		end
	endgenerate

	assign	O1 			= wireO1;
	assign 	SEQ_O1 	= wireO1 >> ($unsigned(BANK_SEL1)*`FM_BANK_COLS);

endmodule
